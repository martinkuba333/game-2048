export const CONST = {
  WINDOW_LISTENER: "keypress",

  SQUARES_NUM: 16, //must be able to sqrt
  SQUARES_SIZE: 50,

  BOARD_TOP_SPACE_SCORE: 60,

  KEY_UP: [119, "up"],
  KEY_RIGHT: [100, "right"],
  KEY_DOWN: [115, "down"],
  KEY_LEFT: [97, "left"],
  KEY_CODE_POSITION: 0,
  KEY_NAME_POSITION:1,

  NUM_INITIAL: 0,
  NUM_NEW: [2, 2, 2, 2, 4],
  NUM_MULTIPLY: 2,

  GAME_OVER_MSG: "GAME OVER",
};

function findStyle(id, wantedStyle) {
  const ele = document.getElementById(id);
  let result = window.getComputedStyle(ele).getPropertyValue(wantedStyle);
  return parseInt(result);
}
