import React, { Component } from "react";
import ReactDOM from "react-dom";
import {CONST} from "./constants";
import {Square, Board} from "./styled";
import {Points} from "./points";

class Main extends Component {
  constructor() {
    super();
    this.state = {
      nums:Array(CONST.SQUARES_NUM).fill(CONST.NUM_INITIAL),
      points: 0,
      moveHappen: false,
      gameEnd: [],
    };
  }

  componentDidMount(){
    this.createNumOnRandomPlace();
    this.createNumOnRandomPlace();
    window.addEventListener(CONST.WINDOW_LISTENER, this.checkKey);
  }

  createNumOnRandomPlace = () => {
    const {nums} = this.state;
    let rand = Math.floor(Math.random() * CONST.SQUARES_NUM);

    while (nums[rand] !== 0) {
      rand = Math.floor(Math.random() * CONST.SQUARES_NUM);
    }

    nums[rand] = CONST.NUM_NEW[Math.floor(Math.random() * CONST.NUM_NEW.length)];
    this.setState({
      numbers:nums,
    });
  };

  checkKey = (e) => {
    switch(e.keyCode) {
      case CONST.KEY_UP[CONST.KEY_CODE_POSITION]:
        this.prepareMoveSquares(CONST.KEY_UP[CONST.KEY_NAME_POSITION]);
        break;
      case CONST.KEY_RIGHT[CONST.KEY_CODE_POSITION]:
        this.prepareMoveSquares(CONST.KEY_RIGHT[CONST.KEY_NAME_POSITION]);
        break;
      case CONST.KEY_DOWN[CONST.KEY_CODE_POSITION]:
        this.prepareMoveSquares(CONST.KEY_DOWN[CONST.KEY_NAME_POSITION]);
        break;
      case CONST.KEY_LEFT[CONST.KEY_CODE_POSITION]:
        this.prepareMoveSquares(CONST.KEY_LEFT[CONST.KEY_NAME_POSITION]);
        break;
    }
  };

  prepareMoveSquares = (move) => {
    const lineLength = Math.sqrt(CONST.SQUARES_NUM);

    let {nums} = this.state;

    let wasCombined = Array(nums.length).fill(false);
    let checkCounter = 0;

    while (checkCounter < lineLength) {
      checkCounter++;

      for (let i = nums.length - 1; i >= 0; i--) {
        if (nums[i] !== CONST.NUM_INITIAL) {

          if (i < CONST.SQUARES_NUM - lineLength) {
            if (nums[i + lineLength] === nums[i] &&
                wasCombined[i + lineLength] === false &&
                wasCombined[i] === false) {
              this.state.gameEnd.push(false);

              if (move === CONST.KEY_DOWN[CONST.KEY_NAME_POSITION]) {
                nums[i + lineLength] = nums[i] * CONST.NUM_MULTIPLY;
                nums[i] = CONST.NUM_INITIAL;
                this.state.moveHappen = true;
                wasCombined[i + lineLength] = true;
                this.updatePoints(nums[i + lineLength]);
              }

            } else if (nums[i + lineLength] === CONST.NUM_INITIAL) {
              this.state.gameEnd.push(false);

              if (move === CONST.KEY_DOWN[CONST.KEY_NAME_POSITION]) {
                nums[i + lineLength] = nums[i];
                nums[i] = CONST.NUM_INITIAL;
                this.state.moveHappen = true;
                wasCombined[i + lineLength] = wasCombined[i];
                wasCombined[i] = false;
              }
            } else {
              this.state.gameEnd.push(true);
            }
          }

          if (i + 1 < CONST.SQUARES_NUM && (i + 1) % lineLength !== 0) {
            if (nums[i + 1] === nums[i] &&
                wasCombined[i + 1] === false &&
                wasCombined[i] === false) {
              this.state.gameEnd.push(false);

              if (move === CONST.KEY_RIGHT[CONST.KEY_NAME_POSITION]) {
                nums[i + 1] = nums[i] * CONST.NUM_MULTIPLY;
                nums[i] = CONST.NUM_INITIAL;
                this.state.moveHappen = true;
                wasCombined[i + 1] = true;
                this.updatePoints(nums[i + 1]);
              }
            } else if (nums[i + 1] === CONST.NUM_INITIAL) {
              this.state.gameEnd.push(false);

              if (move === CONST.KEY_RIGHT[CONST.KEY_NAME_POSITION]) {
                nums[i + 1] = nums[i];
                nums[i] = CONST.NUM_INITIAL;
                this.state.moveHappen = true;
                wasCombined[i + 1] = wasCombined[i];
                wasCombined[i] = false;
              }
            } else {
              this.state.gameEnd.push(true);
            }
          }
        }
      }

      for (let i = 0; i < nums.length; i++) {
        if (nums[i] !== CONST.NUM_INITIAL) {

          if (i > lineLength - 1) {
            if (nums[i - lineLength] === nums[i] &&
                wasCombined[i - lineLength] === false &&
                wasCombined[i] === false) {
              this.state.gameEnd.push(false);

              if (move === CONST.KEY_UP[CONST.KEY_NAME_POSITION]) {
                nums[i - lineLength] = nums[i] * CONST.NUM_MULTIPLY;
                nums[i] = CONST.NUM_INITIAL;
                this.state.moveHappen = true;
                wasCombined[i - lineLength] = true;
                this.updatePoints(nums[i - lineLength]);
              }

            } else if (nums[i - lineLength] === CONST.NUM_INITIAL) {
              this.state.gameEnd.push(false);

              if (move === CONST.KEY_UP[CONST.KEY_NAME_POSITION]) {
                nums[i - lineLength] = nums[i];
                nums[i] = CONST.NUM_INITIAL;
                this.state.moveHappen = true;
                wasCombined[i - lineLength] = wasCombined[i];
                wasCombined[i] = false;
              }
            } else {
              this.state.gameEnd.push(true);
            }
          }

          if (i > 0 && (i - 1) % lineLength !== lineLength - 1) {
            if (nums[i - 1] === nums[i] &&
                wasCombined[i - 1] === false &&
                wasCombined[i] === false) {
              this.state.gameEnd.push(false);

              if (move === CONST.KEY_LEFT[CONST.KEY_NAME_POSITION]) {
                nums[i - 1] = nums[i] * CONST.NUM_MULTIPLY;
                nums[i] = CONST.NUM_INITIAL;
                this.state.moveHappen = true;
                wasCombined[i - 1] = true;
                this.updatePoints(nums[i - 1]);
              }

            } else if (nums[i - 1] === CONST.NUM_INITIAL) {
              this.state.gameEnd.push(false);

              if (move === CONST.KEY_LEFT[CONST.KEY_NAME_POSITION]) {
                nums[i - 1] = nums[i];
                nums[i] = CONST.NUM_INITIAL;
                this.state.moveHappen = true;
                wasCombined[i - 1] = wasCombined[i];
                wasCombined[i] = false
              }
            } else {
              this.state.gameEnd.push(true);
            }
          }
        }
      }
    }

    this.afterMove();
  };

  afterMove = () => {
    const {moveHappen, nums, gameEnd} = this.state;

    if (moveHappen) {
      this.createNumOnRandomPlace();

    } else if (nums.indexOf(CONST.NUM_INITIAL) === -1 && gameEnd.indexOf(false) === -1) {
      this.gameOver();
    }

    this.state.gameEnd = [];
  };

  gameOver = () => {
    alert(CONST.GAME_OVER_MSG);
    window.removeEventListener(CONST.WINDOW_LISTENER, this.checkKey);
  };

  createSquares = () => {
    const {nums} = this.state;
    let squares = [];

    for (let i = 0; i < CONST.SQUARES_NUM; i++) {
      squares.push(
        <Square
          key={i}
          size={CONST.SQUARES_SIZE}
          fontSize={CONST.SQUARES_SIZE / 2}
        >
          {nums[i] !== CONST.NUM_INITIAL ? nums[i] : ""}
        </Square>
      );
    }
    return squares;
  };

  updatePoints = (value = 0) => {
    this.setState({
      points: this.state.points + value,
    });
  };

  createPoints = () => {
    const {points} = this.state;

    return (
      <React.Fragment>
        <Points
          value={points}
        />
      </React.Fragment>
    );
  };

  render() {
    return (
      <React.Fragment>
        <Board
          size={CONST.SQUARES_SIZE * Math.sqrt(CONST.SQUARES_NUM)}
        >
          {this.createPoints()}
          {this.createSquares()}
        </Board>
      </React.Fragment>
    );
  }
}
export default Main;

board ? ReactDOM.render(<Main />, board) : false;
