import React, { Component } from "react";
import {ShowPoints} from "./styled";
import {CONST} from "./constants";

export class Points extends Component {
  constructor() {
    super();
    this.state = {
      points: 0,
    };
  }

  componentWillReceiveProps () {
    if (this.state.points < this.props.value) {
      this.setState({
        points: this.props.value,
      })
    }
  }

  render() {
    const {points} = this.state;

    return (
      <React.Fragment>
        <ShowPoints>
          Points: {points}
        </ShowPoints>
      </React.Fragment>
    );
  }
}
