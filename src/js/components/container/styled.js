import styled from "styled-components";

export const Board = styled.div`
  margin: 20px auto;
  width: ${({size}) => size}px;
  height: ${({size}) => size}px;
  pointer-events:none;
`;

export const Square = styled.div`
  background: #eee;
  box-shadow: inset 0 0 3px 0px black;
  width: ${({size}) => size}px;
  height: ${({size}) => size}px;
  float:left;
  text-align: center;
  font-size: ${({fontSize}) => fontSize}px;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export const ShowPoints = styled.div`
  text-align:center;
  font-size:30px;
`;
